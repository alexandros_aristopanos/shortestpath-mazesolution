class Robot
  ROBOT_CHAR = 'x'
  TRACK_MARKER = 'o'
  FINISH_CHAR = 'f'
  VISITED_TWICE = 'E'
  SPACE_CHAR = ' '
  DIRECTIONS = {"E"=>[0,1] , "W"=>[0,-1], "S"=>[1,0],"N"=>[-1,0]}

#Briskei arxiki thesi sto map kai an elenxei an uparxoun ta aparetita sumbola mesa sto map, thetei to @current m    e ti thesi tou player
  attr_accessor :current
  def findPlayer(maze)
    @maze = maze
    playerFound = false
    finishFound = false
    @maze.each_with_index do |row, rowIndex|
      row.each_with_index do |col, colIndex|
        case col
          when ROBOT_CHAR then
            raise 'More than one player' if playerFound
            playerFound = true
            @playerRow = rowIndex
            @playerCol = colIndex
            @current = [@playerRow,@playerCol]
          when FINISH_CHAR then finishFound = true
        end
      end
    end
    raise 'No player' unless playerFound
  end

#elenxei gia ti epitreptes kiniseis sto maze. Xrisimopoihei to hash me ta directions, kai gia kathe true elenxo     tou canMove? bazei sto delta to array e tin kateuthinsi. px an kinite anatolika mono to deltas = [[0,1]].
  def possibleMovements
    deltas = []
    directions = ["S","E","N","W"]
    directions.each do |direction|
       deltas << DIRECTIONS[direction] if canMove? direction
    end
    deltas
  end #possibleMovements

 #upologizei gia kathe pithano geitona, to kostos se sxesi me tin apostasi apo to terma kai dialegei to mikrotero    . Sugekrimena pernei to deltas apo to possibleMovements kai upologizei ta cost kai ta pernaei se ena array to choi    ce. Apo to choice briskoume ti thesi tis mikroteris timis(kai pio sumferousas)
  # kai tin antistixoume me tin idia thesi sto pinaka tou delta. Etsi kseroume pros ta pou sumferei na kinithoume.
  attr_accessor :real_choice
  def populate_cost(deltas,start_x,start_y,finish_x,finish_y)
      choice = []
      start_dx = start_x-finish_x
      start_dy = start_y-finish_y
    deltas.each do |delta|
      dx,dy = delta[0], delta[1]
      node_dx, node_dy = (dx+@current[0]),(dy + @current[1])
      cost = (((finish_x-node_dx)) + ((finish_y-node_dy))*0.001).abs
      choice << cost
    end
    real_choice = choice.find_index(choice.min)
  end

#koitaei apo tin torini thesi tous geitones kai kathorizei an uparxei keno wste na mporei na kounithei i an upar    xei to f, pou simenei pws brike ti lusi
#episis sto solution pernaei tin timi pou tha tipwsei gia na simeiwthei i lusi me tis suntetagmenes
 def canMove?(direction)
      if @maze[@playerRow + DIRECTIONS[direction][0]][@playerCol + DIRECTIONS[direction][1]] == FINISH_CHAR
      curr = [@playerRow += DIRECTIONS[direction][0]][@playerCol += DIRECTIONS[direction][1]]
   p    @solution = [@playerRow,@playerCol]
      exit
      else
    @maze[@playerRow + DIRECTIONS[direction][0]][@playerCol + DIRECTIONS[direction][1]] == SPACE_CHAR
  end
 end #canMove?

#bazei 'o' sti torini thesi kai simeiwnei me 'x' panw sto labirintho ti nea thesi tou robot. San orisma dexete t    in epikratesteri thesi gia kinisi.
 def nextMove(direction)
  @maze[@playerRow][@playerCol] = TRACK_MARKER
  @maze[@playerRow += direction[0]][@playerCol += direction[1]] = ROBOT_CHAR
  @current = [@playerRow][@playerCol]
 end #nextMove

#o monos tropos gia na kseroume oti mporei na paei pros ta piso ine na epibebaiwsoume pws i thesi pou lambanei a    po to visited ine markarismeni me 'o'
 def canMoveReverse?(direction)
  @maze[@playerRow - direction[0]][@playerCol - direction[1]] == TRACK_MARKER
 end #canMovereverse?

 #bazei 'E' stis theseis apo opou pernaei gia na kseroume apo tou perase duo fores to robot, wste na markaroume p    io eukola ta adieksoda. 
 attr_accessor :possible_current
 def prevMove(direction)
  @maze[@playerRow][@playerCol] = VISITED_TWICE
  @maze[@playerRow -= direction[0]][@playerCol -= direction[1]] = ROBOT_CHAR
 return  @possible_current = [@playerRow][@playerCol]
 end #prevMove

#optika effects gia cool katastaseis
  def visualize(array)
   @visual =  @maze.map { |row| row.join('') }.join("\n")
  end #visualize

end #class
