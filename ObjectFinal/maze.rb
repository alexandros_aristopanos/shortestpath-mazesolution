class Maze
  attr_accessor :array
  def initialize(filename)
    handler = File.open(filename,"r")
    @array = []
    handler.each_line do |c|
      @array << c.chomp.split('')
    end
    handler.close
    end #init

  #optika effects gia cool katastaseis
  def visualize
   maze =  @array.map { |row| row.join('') }.join("\n")
  end
end  #class
