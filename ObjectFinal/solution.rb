load 'robot.rb' #loads robot class
load 'maze.rb' #loads map and converts it to array
class Solver 
attr_accessor :possible_current
def initialize
  begin
   puts "Enter the maze name you want to explore:"
   filename = gets.chomp
   puts "Give the finish coordinates you discovered. First the X axis coordinate:"
   @finish_x = gets.chomp.to_i
   puts "Give the finish coordinates you discovered. Now, your Y axis coordinate:"
   @finish_y = gets.chomp.to_i
   puts "Give your starting potition. First your X coordinate:"
   @start_x = gets.chomp.to_i
   puts "Give your starting potition. Second your Y coordinate:"
   @start_y = gets.chomp.to_i
   @maze = Maze.new(filename).array
   @robot = Robot.new
   @visited = []
   solve
  rescue
    puts "Be careful with your input. Make sure filenames are corrent and you are using integers at coordinates prompts!!"
  end
end

def solve #recursion and steps of algorithm
  begin  #first part will be executed untill there are no spaces to move. Then traceback will go as far back as it is needed, so we can continue.
   @robot.findPlayer(@maze)
   @possible_current = @robot.possibleMovements
   real_choice=@robot.populate_cost(@possible_current,@start_x,@start_y,@finish_x,@finish_y)
   b = @possible_current[real_choice]
   @robot.nextMove(b)
   puts @robot.visualize(@maze)
   @visited << b
    solve
    rescue
   traceback
     end
end

  def traceback #method for tracking our visited areas and moving backwards
      @robot.prevMove(@visited.pop)  if @robot.canMoveReverse? @visited.last
    solve
  end
end #class
b = Solver.new
